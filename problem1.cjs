const fs = require('fs').promises;

function problem1(filePath, noOffiles) {
    // Ensure the directory path is provided
    filePath = "./Random";

    // Create the directory using promises
    fs.mkdir(filePath)
        .then(() => {
            const fileCreationPromises = [];

            // Create promises for writing files
            for (let index = 0; index < noOffiles; index++) {
                const filename = `file${index}.json`;
                const insertDataInFile = {
                    index: index
                };

                const path = `${filePath}/${filename}`;
                const fileCreationPromise = fs.writeFile(path, JSON.stringify(insertDataInFile));
                fileCreationPromises.push(fileCreationPromise);
            }

            // Wait for all file creations to complete
            return Promise.all(fileCreationPromises);
        })
        .then(() => {
            // Add a delay using setTimeout
            return new Promise(resolve => {
                setTimeout(() => {
                    resolve();
                }, 5000);
            });
        })
        .then(() => {
            const fileDeletionPromises = [];

            // Create promises for deleting files
            for (let i = 0; i < noOffiles; i++) {
                const fileDeletionPromise = fs.unlink(`${filePath}/file${i}.json`);
                fileDeletionPromises.push(fileDeletionPromise);
            }

            // Wait for all file deletions to complete
            return Promise.all(fileDeletionPromises);
        })
        .then(() => {
            console.log("Created and Deleted Successfully");
        })
        .catch((error) => {
            console.error(error);
        });
}

module.exports = problem1;









