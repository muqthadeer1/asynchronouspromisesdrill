const fs = require("fs").promises;

function fnProblem2() {
    const filepath = 'lipsum.txt';

    // Read the data from the given file name filepath
    fs.readFile(filepath, 'utf8')
        .then((data) => {   //data required
            let upperCaseContent = data.toUpperCase();
            let upperCaseFile = "upperCase.txt";

            // Write to the upperCase.txt file
            return fs.writeFile(upperCaseFile, upperCaseContent, "utf8");
        })
        .then(() => {
            // Append the upperCase.txt filename to filenames.txt
            return fs.appendFile("filenames.txt", "upperCase.txt\n");
        })
        .then(() => {
            // Read the upperCase.txt file
            return fs.readFile("upperCase.txt", "utf8");
        })
        .then((upperCaseData) => {
            let lowerCase = upperCaseData.toLocaleLowerCase().split('.').join('\n');
            let lowerCaseFile = "lowerCase.txt";

            // Write to the lowerCase.txt file
            return fs.writeFile(lowerCaseFile, lowerCase, "utf8");
        })
        .then(() => {
            // Append the lowerCase.txt filename to filenames.txt
            return fs.appendFile("filenames.txt", "lowerCase.txt\n");
        })
        .then(() => {
            // Read the upperCase.txt and lowerCase.txt files
            return Promise.all([
                fs.readFile("upperCase.txt", "utf8"),
                fs.readFile("lowerCase.txt", "utf8")
            ]);
        })
        .then(([upperCaseData, lowerCaseData]) => {
            let upperCaseSorted = upperCaseData.split(' ').sort().join(' ');
            let lowerCaseSorted = lowerCaseData.split(' ').sort().join(' ');

            let upperAndLowerSorted = upperCaseSorted + '\n' + lowerCaseSorted;
            let upperAndLowerSortedPath = "sortedContent.txt";

            // Write to the sortedContent.txt file
            return fs.writeFile(upperAndLowerSortedPath, upperAndLowerSorted, 'utf8');
        })
        .then(() => {
            // Append the sortedContent.txt filename to filenames.txt
            return fs.appendFile('filenames.txt', 'sortedContent.txt\n');
        })
        .then(() => {
            // Read the filenames.txt file
            return fs.readFile('filenames.txt', 'utf8');
        })
        .then((data) => {
            let numberOfFiles = data.trim().split('\n');

            // Unlink each file mentioned in filenames.txt
            return Promise.all(numberOfFiles.map((eachFile) => fs.unlink(eachFile.trim())));
        })
        .then(() => {
            console.log("Files Created, Appended, Sorted, and Deleted Successfully");
        })
        .catch((error) => {
            console.error(error);
        });
}

module.exports = fnProblem2;
// fnProblem2()
